*** Keywords ***
Login
    #[Arguments]    ${URL}    ${VALID USER}    ${VALID PASSWORD}
    Open Browser    ${URL}    ${BROWSER}
    Maximize Browser Window
    input text    ${USER LOCATION}    ${VALID USER}
    input password    ${PASSWORD LOCATION}    ${VALID PASSWORD}
    click button    ${LOGIN}

C_Login
    #[Arguments]    ${VALID USER}    ${VALID PASSWORD}
    Open Browser    ${C_URL}    ${BROWSER}
    input text    ${USER LOCATION}    ${CLIENT_USER}
    input password    ${PASSWORD LOCATION}    ${CLIENT_PASSWORD}
    click button    ${LOGIN}
    #sleep    2s
    #click element    ${User}
    #sleep    2s
    #click element    ${Sign_Out}

Login1
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    ${URL}
    input text    ${USER LOCATION}    ${VALID USER}
    input password    ${PASSWORD LOCATION}    ${VALID PASSWORD}
    click button    ${LOGIN}

C_Login1
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    ${C_URL}
    input text    ${USER LOCATION}    water
    input password    ${PASSWORD LOCATION}    Water@1011
    click button    ${LOGIN}

Test
    Open Browser    ${URL}    Firefox

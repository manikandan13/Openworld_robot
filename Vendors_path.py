Add_New_Vendor= "//button[contains(text(),'Add New Vendor')]"
Vendor_search= "//input[@id='column0']"
Vendor_Serbut= "id=searchBtn"
Vendor_Name= "//input[@ng-model='vendor.name']"
Vendor_Description= "//input[@ng-model='vendor.description']"
Vendor_contactNo= "//input[@id='contactNo']/following-sibling::div/input"
Vendor_faxNo= "//input[@id='faxNo']/following-sibling::div/input"
Vendor_Website= "//input[@ng-model='vendor.url']"
Vendor_pincode= "//input[@id='pincode']"
vendor_State= "//input[@id='vendorState']"
vendor_City= "//input[@id='vendorCity']"
Vendor_Area= "//input[@ng-model='vendor.area']"
Vendor_Address= "//input[@ng-model='vendor.address']"
Vendor_Add_User= "//span[contains(text(),'Add User')]"
Vendor_Add_Vendor= "//button[contains(text(),'Add Vendor')]"
Vendor_checks= "//div[@class='col-sm-6 ng-binding ng-scope']"
Vendor_checks_box= "//div/input[@type='checkbox']"
Vendor_Firstname= "//input[@id='firstName']"
Vendor_Lastname= "//input[@ng-model='clientUser.lastName']"
Vendor_Designation= "//input[@ng-model='clientUser.designation']"
Vendor_Email= "//input[@id='userEmail']/following-sibling::div/input"
Vendor_Phoneno= "//input[@id='userPhone']/following-sibling::div/input"
Vendor_Faxno= "//input[@id='userFax']/following-sibling::div/input"
Vendor_Username= "//input[@ng-model='clientUser.username']"
Vendor_password= "//input[@ng-model='clientUser.password']"
Vendor_con_password= "//input[@id='confirmPassword']"
Vendor_User_Pincode= "//input[@id='userPincode']"
Vendor_User_state= "//input[@ng-model='clientUser.state']"
Vendor_User_city= "//input[@ng-model='clientUser.city']"
Vendor_User_Area= "//input[@ng-model='clientUser.area']"
Vendor_User_Address= "//input[@ng-model='clientUser.address']"
Vendor_Create= "//button[text()='Create Vendor']"
Vendor_User_Add= "//button[@class='btn save-button']"
Vendor_User_cancel= "//button[text()='Cancel']"
Vendor_county_input= "//*[@id='userCountry_chzn']/div/div/input"
Vendor_county_slect= "//*[@id='userCountry_chzn']/div/ul/li"
Vendor_Edit= "//span[@class='glyphicon glyphicon-pencil']"
Vendor_Name_valid= "//input[@class='form-control ng-isolate-scope ng-scope ng-dirty ng-invalid ng-invalid-required err']"




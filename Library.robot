*** Settings ***
Library           Selenium2Library
Library           String
Library           Collections
Variables         Case_Reg_Values.py
Resource          Login.robot
Variables         Case_Reg_Alerts.py
Variables         DataEntry_Path.py
Variables         DataEntry_Values.py
Variables         CaseManager_Path.py
Variables         CaseManager_Values.py
Variables         Master_Path.py
Variables         Master_Values.py
Variables         Gmail_path.py
Variables         Gmail_values.py
Variables         Login_values.py
Variables         Login_Path.py
Variables         Login_Alerts.py
Variables         Case_Reg_Path.py
Resource          Case_Registration.robot
Variables         values.py
Variables         Side_Bar_Path.py
Variables         Document_type_path.py
Variables         Document_type_value.py
Variables         Document_type_alert.py
Variables         Master_Path.py
Variables         Vendors_path.py
Variables         Vendors_Alert.py
Resource          Vendor.robot
Resource          Common.robot
Resource          Gmail.robot
Variables         Contract_path.py
Variables         Contract_Alert.py
Variables         CaseManager_Alerts.py
Variables         client_master_path.py
Variables         client_master_values.py
Resource          Contract.robot
Library           DateTime
Variables         Case_Assign_Path.py
Resource          Case_Assingment.robot

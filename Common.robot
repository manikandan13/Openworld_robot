*** Keywords ***
Random String
    ${RANDOM_STRING}    Generate Random String    8    abcdefghijklmnopqrstuvwxyz
    ${TEMP_FIRST_NAME}    Generate Random String    5    abcdefghijklmnopqrstuvwxyz
    ${TEMP_LAST_NAME}    Generate Random String    1    abcdefghijklmnopqrstuvwxyz
    ${RANDOM_NUMBER}    Generate Random String    13    123456789012
    #${RANDOM_SYMBOLS}    Generate Random String    5    ~!@#$%^&*(){}[]|><
    Set Global Variable    ${RANDOM_STRING}
    Set Global Variable    ${TEMP_FIRST_NAME}
    Set Global Variable    ${TEMP_LAST_NAME}
    Set Global Variable    ${RANDOM_NUMBER}
    #Set Global Variable    ${RANDOM_SYMBOLS}

Random String Search values
    ${firstletter}=    Get substring    ${TEMP_FIRST_NAME}    0    1
    ${upperletter}=    Convert To Uppercase    ${firstletter}
    ${nextletters}=    Get substring    ${TEMP_FIRST_NAME}    1
    ${FIRST_NAME}=    Catenate    ${upperletter}${nextletters}
    ${LAST_NAME}=    Convert To Uppercase    ${TEMP_LAST_NAME}
    Set Global Variable    ${FIRST_NAME}
    Set Global Variable    ${LAST_NAME}
    #Log To Console    ${FIRST_NAME}
    #Log To Console    ${LAST_NAME}

Date validation
    ${curr_date} =    Get Current Date
    #log to console    ${date}
    ${date} =    Add Time To Date    ${curr_date}    8 days
    ${Due_Date} =    Convert Date    ${date}    result_format=%d/%m/%Y
    #log to console    ${date}
    Set Global Variable    ${Due_Date}

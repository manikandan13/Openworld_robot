Contract_save_alert= 'The updated contract will be applicable ONLY for cases registered from now on. All live cases will use the previously saved contract.'
Contract_success_alert= 'Contract successfully saved'
Without_ID_Any_alert= "A minimum of 2 ID's has to be selected"
Without_ID_scl_alert= 'Please select at least any one ID.'

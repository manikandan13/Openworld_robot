*** key words ***
Check Select
    ${Check_Box}=    set variable    ${Case_Asign_Check}
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    click element    xpath=(${Check_Box})[${INDEX}]

Select the Case Assignment Option
    click element    ${Case_Registration}
    click element    ${Case_Manager}
    sleep    2s
    click element    ${Case_Asign}

Select the Vendor
    click element    ${Case_Vendor_Secl}

Assing the case to the Vendor
    click element    ${Case_Assign_btn}

Select the Client and the Project
    sleep    3s
    click element    ${Case_CliPro}
    sleep    2s
    input text    ${Case_Clisel}    ${client1}
    sleep    0.5s
    Press key    ${Case_Clisel}    \\13
    sleep    2s
    input text    ${Case_Clisel}    ${Uni_Project1}
    sleep    0.5s
    Press key    ${Case_Clisel}    \\13

Select the Check for Case Assign
    sleep    3s
    ${Check_Count}=    Get matching xpath count    ${Case_Asign_Text}
    Set Global Variable    ${Case_Asign_Text}
    Log To Console    ${Check_Count}
    @{Check_List} =    Create List    Education    Reference    Address    Criminal    Employment
    ...    ID    Database
    Set Global Variable    ${Check_List}
    ${Check_count}    Get Length    ${Check_List}
    Log To Console    ${Check_count}
    Set Global Variable    ${Check_count}
    : FOR    ${INDEX}    IN RANGE    1    ${Check_count}+1
    \    Set Global Variable    ${INDEX}
    \    ${Checks}=    Get Text    xpath=(${Case_Asign_Text})[${INDEX}]
    \    Run Keyword If    '${Checks}' == ${Check_List}[${INDEX}-1]    Check Select
    sleep    2s

Search the Case by refno name clientID
    input text    ${Case_AS_Search}    DIR000048

Search the Case
    click element    ${Case_Search}

Clear the Case
    click element    ${Case_Clear}

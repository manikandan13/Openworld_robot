URL = 'http://192.168.2.17:5000/'
C_URL = 'http://192.168.2.17:5000/client'
USER_LOCATION= "xpath=//input[@type='text']"
PASSWORD_LOCATION= "xpath=//input[@type='password']"
LOGIN= "xpath=//button[@type='submit']"
FORGOT_PASSWORD= "xpath=//p[contains(text(),'Forgot Password?')]"
FORGOT_PASSWORD_YES= "xpath=//button[contains(text(),'Yes')]"
FORGOT_PASSWORD_NO= "xpath=//button[contains(text(),'No')]"
NEW_PASSWORD_LOCATION= 'id=newPassword'
CONFIRM_PASSWORD_LOCATION= 'id=confirmPassword'
CONFIRM_BUTTON= "xpath=//button[@type='submit']"
PASSWORD_ALERT= "xpath=//button[contains(text(),'OK')]"
User= "//i[@class='fa fa-user']"
Sign_Out= "//a[@class='btn signout-btn']"

*** Settings ***
Resource          Library.robot

*** Test Cases ***
To validate for the stop case re initiate in the case registration
    Close Browser
    sleep    3s
    Login
    Random String
    sleep    3s
    click element    ${Case_Registration}
    wait until element is enabled    ${Register_New_Case}
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service Provider}
    click element    ${Service Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Father_Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    wait until element is enabled    ${HR_MailID}
    click element    ${HR_MailID}
    input text    ${HR_MailID}    ${RANDOM_STRING}@gmail.com
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    wait until element is enabled    ${Save_Submit}
    click element    ${Save_Submit}
    sleep    2s
    Element Should Contain    ${Alert_Message}    ${Save_submit_Alert}
    Click Element    ${Save_ok}
    #re initiing the case for second time
    sleep    3s
    click element    ${Case_Registration}
    wait until element is enabled    ${Register_New_Case}
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service Provider}
    click element    ${Service Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Father_Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    wait until element is enabled    ${HR_MailID}
    click element    ${HR_MailID}
    input text    ${HR_MailID}    ${RANDOM_STRING}@gmail.com
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    wait until element is enabled    ${Save_Submit}
    click element    ${Save_Submit}
    sleep    2s
    Element Should Contain    ${Alert_Message}    ${Email_Alert}
    Click Element    ${Save_ok}
    sleep    3s
    click element    ${CaseManager}
    sleep    2s
    click element    ${Conformation_Alert_yes}
    sleep    2s
    click element    ${CaseManager}
    click element    ${Case_Search_Text}
    input text    ${Case_Search_Text}    ${TEMP_FIRST_NAME}
    click element    ${Case_Search}
    sleep    2s
    wait until element is enabled    ${Stop_case}
    click element    ${Stop_case}
    wait until element is enabled    ${Stop_Case_Comment}
    #//button[text()='Stop Case']
    click element    ${Stop_Case_Comment}
    input text    ${Stop_Case_Comment}    ${Stop_Comment}
    sleep    2s
    click element    ${Stop_Case_button}
    #re initiing the case for third time after stop case
    sleep    3s
    click element    ${Case_Registration}
    wait until element is enabled    ${Register_New_Case}
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service Provider}
    click element    ${Service Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Father_Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    wait until element is enabled    ${HR_MailID}
    click element    ${HR_MailID}
    input text    ${HR_MailID}    ${RANDOM_STRING}@gmail.com
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    wait until element is enabled    ${Save_Submit}
    click element    ${Save_Submit}
    sleep    2s
    Element Should Contain    ${Alert_Message}    ${Save_submit_Alert}
    Click Element    ${Save_ok}

To validate for the Service provider notification mail ID auto populate from the DB and its accept n-number of ID's
    Close Browser
    sleep    3s
    Login
    Random String
    Random String Search values
    sleep    3s
    click element    ${Case_Registration}
    wait until element is enabled    ${Register_New_Case}
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Last_Name_Location}    ${TEMP_LAST_NAME}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${TEMP_FIRST_NAME}
    input text    ${Father_Last_Name_Location}    ${TEMP_LAST_NAME}
    #sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${Landline_Number}
    input text    ${Mobile_Number_Location}    ${Mobile_Number}
    click element    ${Priority}
    click element    ${Priority_Select}
    sleep    2s
    wait until element is enabled    ${HR_MailID}
    click element    ${HR_MailID}
    #input text    ${HR_MailID}    ${RANDOM_STRING}@gmail.com
    sleep    7s
    Element Should Contain    ${HR_MailID}    ${mail_Autopopulate1}
    Element Should Contain    ${HR_MailID}    ${mail_Autopopulate2}
    Close Browser

To validate for the Stop Case.
    Close Browser
    sleep    3s
    Login
    sleep    3s
    click element    ${CaseManager}
    click element    ${Case_Search_Text}
    input text    ${Case_Search_Text}    The King
    click element    ${Case_Search}
    #wait until element is enabled    ${Stop_case}
    #click element    ${Stop_case}
    #wait until element is enabled    ${Stop_Case_Comment}
    #//button[text()='Stop Case']
    #click element    ${Stop_Case_Comment}
    #input text    ${Stop_Case_Comment}    ${Stop_Comment}
    #click element    ${Stop_Case_button}
    #sleep    3s
    #click element    ${Case_Modify}
    #click element    ${Add_Component}
    #click element    ${Delete_Component}
    close Browser

TC_00_ID contract
    [Documentation]    To validate for the ID contarct is working based on the requirements.
    #Set Selenium Speed    2s
    login
    sleep    3s
    click element    ${Client}
    sleep    3s
    click element    ${Contract}
    sleep    3s
    click element    //*[@id='client_chzn']/a/span
    input text    //*[@id='client_chzn']/div/div/input    ${client1}
    sleep    3s
    click element    ${DB_Cli_Selc}
    click element    ${ID_contract}
    sleep    3s
    click element    ${Specific}
    ${test}    Get Text    //div[@class='ng-binding ng-scope']/input[@class='id ng-pristine ng-valid']
    Log To Console    ${test}
    ID Selection
    sleep    2s
    click element    ${Save}
    sleep    2s
    Element Should Contain    ${Alert_Message}    ${Contract_save_alert}
    sleep    3s
    click element    ${Conformation_Alert_yes}
    sleep    2s
    Element Should Contain    ${Alert_Message}    ${Contract_success_alert}
    sleep    2s
    Click Element    ${Save_ok}
    Close all browsers

Template
    #[Arguments]    ${Client/project}    ${FIRST_NAME}    ${LAST_NAME}    ${Email}    ${Father_FIRST_NAME}    ${Father_LAST_NAME}
    #...    ${Employe_ID}
    Random String
    Random String Search values
    sleep    3s
    click element    ${Case_Registration}
    wait until element is enabled    ${Register_New_Case}
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client/project}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FIRST_NAME}
    input text    ${Last_Name_Location}    ${LAST_NAME}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${Email}
    input text    ${Father_First_Name_Location}    ${Father_FIRST_NAME}
    input text    ${Father_Last_Name_Location}    ${Father_LAST_NAME}
    #sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    wait until element is enabled    ${Save_Submit}
    click element    ${Save_Submit}
    wait until element is enabled    ${Alert_Message}
    Element Should Contain    ${Alert_Message}    ${Initiatemail_Alert}
    Click Element    ${Save_ok}

*** key words ***
Gmail
    open Browser    ${G_URL}    ${BROWSER}
    Maximize Browser Window
    input text    ${GMAIL_LOCATION}    ${GMAIL_ID}
    click element    ${ID_NEXT}
    Wait Until Element Is Visible    ${GMAIL_PASSWORD_LOCATION}
    input text    ${GMAIL_PASSWORD_LOCATION}    ${GMAIL_PASSWORD}
    click element    ${PASSWORD_NEXT}
    Wait Until Element Is Visible    ${E_SEARCH}    10s
    click element    ${E_SEARCH}
    input text    ${E_SEARCH}    ${Gmail_Search}
    sleep    5s
    click element    ${E_S_CLICK}
    Wait Until Element Is Visible    ${E_IN_LINK}    15s
    click element    ${E_IN_LINK}
    wait Until Element Is Visible    ${Setup_Password}
    click element    ${Setup_Password}
    Select Window    title=Checks360
    Wait Until Element Is Visible    ${NEW_PASSWORD_LOCATION}
    Page Should Contain Element    ${NEW_PASSWORD_LOCATION}
    input text    ${NEW_PASSWORD_LOCATION}    ${NEW_PASSWORD}
    input text    ${CONFIRM_PASSWORD_LOCATION}    ${CONFIRM_PASSWORD}
    click element    ${CONFIRM_BUTTON}
    wait until element is enabled    ${Password_Alert}
    click element    ${Password_Alert}

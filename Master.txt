*** Settings ***
Default Tags      Master
Resource          Library.robot

*** Test Cases ***
TC_01_Document type master
    [Documentation]    To validate for the Document type master whether it works based on the requirements.
    Set Selenium Speed    0.5s
    login
    click element    ${Masters}
    click element    ${Document_Types}
    click element    ${Doc_Search}
    input text    ${Doc_Search}    General
    click element    ${Doc_Gen_edit}
    click element    ${Doc_Document_type}
    input text    ${Doc_Document_type}    ${Document_Type_1}
    log to console    ${Document_Type_1}
    click element    ${Doc_Add}
    Element Should Contain    ${Alert_Message}    ${Doc_Add_alert}
    Click Element    ${Save_ok}
    sleep    3s
    click element    xpath=(${Doc_Search})[2]
    input text    xpath=(${Doc_Search})[2]    ${Document_Type_1}
    click element    ${Doc_Edit}
    input text    ${Doc_Document_type}    ${Document_Type_2}
    log to console    ${Document_Type_2}
    click element    ${Doc_Delete}
    Element Should Contain    ${Alert_Message}    ${Doc_Del_alert}
    click Element    ${Conformation_Alert_yes}
    Close all browsers

TC_02_ID Master BS
    [Documentation]    To validate for the ID master wether it show the as per the requirments before sorting
    Set Selenium Speed    0.5s
    login
    click element    ${Masters}
    click element    ${ID}
    ${Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{Before_sort} =    Create List    Aadhaar Card    Driving Licence    National ID Card    National Insurance Card    PAN Card
    ...    Passport    Ration card    Social Insurance Number    Social Security Number    Voters ID
    Set Global Variable    ${Before_sort}
    ${ID_count}    Get Length    ${Before_sort}
    Log To Console    ${ID_count}
    Set Global Variable    ${ID_count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${ID_s}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${ID_s}' != ${Before_sort}[${INDEX}-1]    Fail
    Close all browsers

TC_03_ID Master AS
    [Documentation]    To validate for the ID master whether it show the as per the requirments after sorting
    Set Selenium Speed    0.5s
    login
    click element    ${Masters}
    click element    ${ID}
    click element    ${ID_sorting}
    ${Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{After_sort} =    Create List    Voters ID    Social Security Number    Social Insurance Number    Ration card    Passport
    ...    PAN Card    National Insurance Card    National ID Card    Driving Licence    Aadhaar Card
    Set Global Variable    ${After_sort}
    ${ID_count}    Get Length    ${After_sort}
    Log To Console    ${ID_count}
    Set Global Variable    ${ID_count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${ID_s}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${ID_s}' != ${After_sort}[${INDEX}-1]    Fail
    Close all browsers

TC_04_ID Search
    [Documentation]    To validate for the ID master whether the search using ID is working based on the requirements.
    Set Selenium Speed    0.5s
    login
    click element    ${Masters}
    click element    ${ID}
    click element    ${ID_Option}
    input text    ${ID_Text}    Aadhaar Card
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    India
    click element    ${ID_Option}
    input text    ${ID_Text}    Driving Licence
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    All countries
    click element    ${ID_Option}
    input text    ${ID_Text}    National ID Card
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    All countries
    click element    ${ID_Option}
    input text    ${ID_Text}    National Insurance Card
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    United Kingdom
    click element    ${ID_Option}
    input text    ${ID_Text}    PAN Card
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    India
    click element    ${ID_Option}
    input text    ${ID_Text}    Passport
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    All countries
    click element    ${ID_Option}
    input text    ${ID_Text}    Ration card
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    India
    click element    ${ID_Option}
    input text    ${ID_Text}    Social Insurance Number
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    Canada
    click element    ${ID_Option}
    input text    ${ID_Text}    Social Security Number
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    USA
    click element    ${ID_Option}
    input text    ${ID_Text}    Voters ID
    click element    ${Client_Select}
    click element    ${ID_search}
    Element Should Contain    ${ID_Match}    India
    Close all browsers

TC_05_Country Search
    [Documentation]    To validate for the ID master whether the search using Country is working based on the requirements.
    Set Selenium Speed    0.5s
    login
    click element    ${Masters}
    click element    ${ID}
    # All country check
    click element    ${ID_ctry_Option}
    input text    ${ID_txt_Con}    All countries
    click element    ${ID_Sea_Sel}
    click element    ${ID_search}
    ${ID_Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{ID_List1} =    Create List    Driving Licence    National ID Card    Passport
    Set Global Variable    ${ID_List1}
    ${ID_counts}    Get Length    ${ID_List1}
    Log To Console    ${ID_counts}
    Set Global Variable    ${ID_counts}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${con_check}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${con_check}' != ${ID_List1}[${INDEX}-1]    Fail
    # India check
    click element    ${ID_ctry_Option}
    input text    ${ID_txt_Con}    India
    click element    ${ID_Sea_Sel}
    click element    ${ID_search}
    ${ID_Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{ID_List1} =    Create List    Aadhaar Card    Driving Licence    National ID Card    PAN Card    Passport
    ...    Ration card    Voters ID
    Set Global Variable    ${ID_List1}
    ${ID_counts}    Get Length    ${ID_List1}
    Log To Console    ${ID_counts}
    Set Global Variable    ${ID_counts}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${con_check}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${con_check}' != ${ID_List1}[${INDEX}-1]    Fail
    # Canada check
    click element    ${ID_ctry_Option}
    input text    ${ID_txt_Con}    Canada
    click element    ${ID_Sea_Sel}
    click element    ${ID_search}
    ${ID_Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{ID_List1} =    Create List    Driving Licence    National ID Card    Passport    Social Insurance Number    Social Security Number
    Set Global Variable    ${ID_List1}
    ${ID_counts}    Get Length    ${ID_List1}
    Log To Console    ${ID_counts}
    Set Global Variable    ${ID_counts}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${con_check}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${con_check}' != ${ID_List1}[${INDEX}-1]    Fail
    # USA check
    click element    ${ID_ctry_Option}
    input text    ${ID_txt_Con}    USA
    click element    ${ID_Sea_Sel}
    click element    ${ID_search}
    ${ID_Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{ID_List1} =    Create List    Driving Licence    National ID Card    Passport    Social Security Number
    Set Global Variable    ${ID_List1}
    ${ID_counts}    Get Length    ${ID_List1}
    Log To Console    ${ID_counts}
    Set Global Variable    ${ID_counts}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${con_check}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${con_check}' != ${ID_List1}[${INDEX}-1]    Fail
    # United Kingdom check
    click element    ${ID_ctry_Option}
    input text    ${ID_txt_Con}    United Kingdom
    click element    ${ID_Sea_Sel}
    click element    ${ID_search}
    ${ID_Count}=    Get matching xpath count    ${ID_Select}
    Set Global Variable    ${ID_Select}
    @{ID_List1} =    Create List    Driving Licence    National ID Card    National Insurance Card    Passport
    Set Global Variable    ${ID_List1}
    ${ID_counts}    Get Length    ${ID_List1}
    Log To Console    ${ID_counts}
    Set Global Variable    ${ID_counts}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${con_check}=    Get Text    xpath=(${ID_Select})[${INDEX}]
    \    Run Keyword If    '${con_check}' != ${ID_List1}[${INDEX}-1]    Fail
    Close all browsers

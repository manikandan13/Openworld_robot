*** keywords ***
ID Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='id'"]/ul/div
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}
    \    Set Global Variable    ${ID}
    \    Run Keyword If    '${ID}' == 'Aadhaar Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Driving Licence'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Voters ID'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'National Insurance Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'National ID Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'PAN Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Passport'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Ration card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Social Security Number'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Social Insurance Number'    Run Keyword    ID_Check BOX

ID_Check BOX
    ${Check_Box}=    set variable    //div[@ng-if="check.checkname=='id'"]/ul/div/input    #//input[@ng-model='compo.selected']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]
